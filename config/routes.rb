Rails.application.routes.draw do
  resources :question_answers
  resources :quizzes
  root 'home#index'
end
