class CreateQuestionAnswers < ActiveRecord::Migration[5.2]
  def change
    create_table :question_answers do |t|
      t.string :question
      t.string :answer
      t.string :attempted_answer
      t.boolean :hide_answer, default: true
      t.references :quiz, foreign_key: true

      t.timestamps
    end
  end
end
