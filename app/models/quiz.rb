class Quiz < ApplicationRecord
  has_many :question_answers

  validates_uniqueness_of :token
end
