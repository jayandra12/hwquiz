json.extract! quiz, :id, :token, :created_at, :updated_at
json.url quiz_url(quiz, format: :json)
