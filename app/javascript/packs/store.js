import Vue from 'vue/dist/vue.esm';
import Vuex from 'vuex';
import Axios from 'axios'

Vue.use(Vuex);
export default new Vuex.Store({
    state: {
        quiz_question_answers: [],      //{quiz: 1, qa: [{id: "1", question: "q1", answer: "a1"}, {id: "2", question: "q2", answer: "a2"}]}
        is_loading: false,
        has_error: false,
        base_url: "localhost:3000",
        loading: false
    },
    getters:{
        all_question_answers: state => {
            return state.quiz_question_answers
            // send ajax to get all questions and cache the result in quiz_question_answers
        },
        find_quiz_by_token: (state) => (token) => {
            if(state.quiz_question_answers.find(q => q.token === token)){
                return state.quiz_question_answers.find(q => q.token === token);
            }else{
                var path = "/quizzes/"+token+".json";

                Axios.get(path)
                  .then((response)  =>  {
                    this.loading = false;
                    state.quiz_question_answers.push(response.data);
                  }, (error)  =>  {
                    this.loading = false;
                    alert("something went wrong in getting quiz details");
                   });
            }
        },
        find_question_answer_by_id: (state) => (quiz_token, id) => {
            var quiz = this.find_quiz_by_token(quiz_token);
            if(quiz.question_answers.find(qa => qa.id === id)){
                return quiz.question_answers.find(qa => qa.id === id);
            }
        }
    },
    mutations: {
        add_question_answer(state, payload){
            state.quiz_question_answers.push(payload);
        },
        update_question_answer(state, payload){
            var qa = state.quiz_question_answers.find(qa => qa.id === payload.id);
            console.log(qa);
        },
        delete_question_answer(state, payload){
            var qa = store.find_by_id(payload.id);
            // TODO
        }
    }
});