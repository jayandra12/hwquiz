import Vue from 'vue/dist/vue.esm'
import VueRouter from 'vue-router';


import qa_show from "../components/quiz.vue"


Vue.use( VueRouter );

export default new VueRouter({
    routes: [
        {path: '/quizzes/:token', component: qa_show}
    ]
})